<?php

namespace Drupal\bbma_navbar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Render\Markup;

/**
 * Provides a 'BbmaNavbarBlock' block.
 *
 * @Block(
 *  id = "bbma_navbar_block",
 *  admin_label = @Translation("Biblissima navbar"),
 * )
 */
class BbmaNavbarBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    if (stream_resolve_include_path('top-bar.php')) {
      ob_start();
      require_once('top-bar.php');
      $topbar = ob_get_clean();
      $build['bbma_navbar_block']['#markup'] = Markup::create($topbar);
    }

    return $build;
  }

}
